package modulo2;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		byte 		bmin = -128;
		byte 		bmax = 127;
//		reemplazar el 0 por el valor que corresponda en todos los caso
		short 	smin = (short)-32768;
		short 	smax = (short)32767;
		int 		imax = 2147483647;
		int 		imin = -2147483648;
		long 		lmin = -2^(63);
		long 		lmax = 2^(63)-1;

		System.out.println("tipo\tminimo\t\tmaximo");
		System.out.println("....\t......\t\t......");
		System.out.println("\nbyte\t" + bmin + "\t\t" + bmax);
		System.out.println("\nshort\t" + smin + "\t\t" + smax);
		System.out.println("\nint\t" + imin + "\t" + imax);
		System.out.println("\nlong\t" + "-2^(63)" + "\t\t" + "2^(63)-1");
		System.out.println("la formula usada es  Max = 2^n-1  Max = -2^n");
        
	}

}
