package practica4;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("\nEjercicio 2 ");
		System.out.println("Ingrese un numero");
		int numero = scan.nextInt();
	
		if (numero % 2 == 0)
			System.out.println("Par");
		else 
			System.out.println("Impar");
	}

}
