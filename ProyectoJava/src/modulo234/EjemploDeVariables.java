package modulo234;

public class EjemploDeVariables {

	public static void main(String[] args) {
		// las variables sirven para guardar informacion
	    
		int cantidad1 = 8;
		int cantidad2 = 40;
		int cantidad3 = 65;
		int cantidad = 9 ;
		int cantTotal = cantidad1 +
				       cantidad2 +
				       cantidad3 ;
		
		float promCant = cantTotal/3 ;
		float temperatura = 23.3f;
	    
	    boolean isAlerta = false;
	    
	    System.out.println("la cantidad es =\t" + cantidad);
	    System.out.println("la temperatura es =\t" + temperatura);
	    System.out.println("isAlerta es =\t\t" + isAlerta);
	    System.out.println("\ncantidad total =\t" + cantTotal);
	    System.out.println("promedio =\t\t" + promCant);
	}

}
